package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestBooking {
    /**
     * Test case on finding the hotel by name on specific dates
     */
    @Test
    public void testBooking() {
        System.setProperty("webdriver.chrome.driver", "/Users/aygulmalikova/Downloads/chromedriver");

        WebDriver driver = new ChromeDriver();
        driver.get("http://www.booking.com");
        System.out.println("Page Title is " + driver.getTitle());
        Assert.assertEquals("Booking.com | Official site | The best hotels & accommodation", driver.getTitle());

        // find search field and enter the string
        WebElement findLine = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/div[1]/div[1]/div[1]/label/input"));
        findLine.sendKeys("Burj Al Arab Jumeirah");

        // find check in field and click on it to open the table with calendar
        WebElement checkInField = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/div[2]/div[1]/div[2]/div/div/div/div/span"));
        checkInField.click();

        // click on the check in day (3rd March)
        WebElement checkInDay = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/div[2]/div[2]/div/div/div[3]/div[2]/table/tbody/tr[1]/td[3]"));
        checkInDay.click();

        // click on the check out day (11th March)
        WebElement checkOutDay = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/div[2]/div[2]/div/div/div[3]/div[2]/table/tbody/tr[2]/td[4]"));
        checkOutDay.click();

        // find and click on the search button
        WebElement searchButton = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/form/div[1]/div[4]/div[2]/button"));
        searchButton.click();

        // find the first element in the list and it has to match the entered one
        WebElement firstFoundHotel = driver.findElement(By.xpath("/html/body/div[3]/div/div[3]/div[1]/div[1]/div[6]/div[4]/div[1]/div/div[1]/div[2]/div[1]/div[1]/div[1]/h3/a/span[1]"));
        Assert.assertEquals("Burj Al Arab Jumeirah", firstFoundHotel.getText());

        // check that check-in date was set correctly
        WebElement actualCheckInDate = driver.findElement(By.xpath("/html/body/div[3]/div/div[3]/div[1]/div[2]/div[1]/div[1]/form/div[3]/div/div[1]/div[1]/div/div/div/div[2]"));
        Assert.assertEquals("Wednesday 3 March 2021", actualCheckInDate.getText());

        // check that check-out date was set correctly
        WebElement actualCheckOutDate = driver.findElement(By.xpath("/html/body/div[3]/div/div[3]/div[1]/div[2]/div[1]/div[1]/form/div[3]/div/div[1]/div[2]/div/div/div/div[2]"));
        Assert.assertEquals("Thursday 11 March 2021", actualCheckOutDate.getText());

        driver.quit();
    }
}